%Fix FAS-905 emg

% Tested version of code to adjust emg channel order for FAS-905 where the
% first half of the capture used 16 emg channels and the second half used 
% emg 17 channels. 

% This code copies the 17th channel (Soleus), pasting it in place of the
% 5th channel (soleus not working) and adjusts the headers accordingly so
% that the user is left with a nice square dataset of 16 channels all
% round!

%% Define working folders and subjects
cohortID = 'FAI';
% Starting folder for input and elaboration files
mocapFolder = 'C:\Users\s5001683\PhD\Mocap';
% Starting folder for input and elaboration files
startingFolder = ([mocapFolder filesep cohortID]);
dirElab = 'C:\Users\s5001683\PhD\Mocap\FAI\Baseline\3_Cleaned\ElaboratedData';
% dirElab = uigetdir(startingFolder,'Select ELABORATED Data Folder');
dirInput = regexprep(dirElab, 'ElaboratedData', 'InputData');
SessionID = 'Session_1'; %'Session_1'; 'Session_2';

% %------------------------------------------------------------------------------------------------
% %% SELECT SUBJECTS TO PROCESS
% %----------------------------
% % % To Batch Process all subjects in an Elaborated folder:
% [subjectNames] = getSubjectTrialData(dirElab);
% To Process particular subjects in an Elaborated folder:
[subjectNames] = {'FAS-905'};%;'M03';'M04';'M05'}; % <-- list subjects you want to process here separated by a colon

%% EMG LABEL SETTINGS
% Note that depending on the capture these might change between the full
% labels and the truncated labels

% Full emg labels
% EMGLabels = {}

% Truncated emg labels
EMGLabels = {'Voltage.BicepsFe','Voltage.Semitend','Voltage.LateralG',...
            'Voltage.MedialGa','Voltage.Soleus','Voltage.GlutMax',...
            'Voltage.GlutMed','Voltage.TFL','Voltage.VastLat',...
            'Voltage.RecFem','Voltage.VastMed','Voltage.Adductor',...
            'Voltage.TibAnt','Voltage.Peroneal','Voltage.Wireless','Voltage.Direct S'};

%% Main body of script
% Cycles through each trial in the working directory meeting the above
% conditions, finds instances where there are more than 17 channels and
% corrects the order based on channel 17 replacing channel 5. Where there
% are 16 channels replaces all labels with the correct emg labels (see EMG
% label settings above)
for n = 1:length(subjectNames)
    dirSession = ([dirElab filesep subjectNames{n} filesep SessionID filesep 'sessionData']);
    trialList = getSubjectTrialData(dirSession);
    for xx = 1:length(trialList)
        dirAnalogdata = ([dirSession filesep trialList{xx}]);
        AnalogData = importdata([dirAnalogdata filesep 'AnalogData.mat']);
        if size(AnalogData.RawData,2) == 17;
            AnalogData.RawData(:,5) = AnalogData.RawData(:,17);
            AnalogData.RawData(:,17) = [];
            %AnalogData.Labels(:,5) = AnalogData.Labels(:,17);
            AnalogData.Labels(:,17) = [];
            AnalogData.Labels = EMGLabels;
            AnalogData.Units(:,17) = [];
        elseif size(AnalogData.RawData, 2) == 16
            AnalogData.RawData(:,5) = 0.001; %set to minimum value for squats for FAS-901 baseline
            AnalogData.Labels = EMGLabels;
        end
        save([dirAnalogdata filesep 'AnalogData.mat'],'AnalogData');
    end
end

disp('Code completed')

%% T.N.Savage CMR 2017