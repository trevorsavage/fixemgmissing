function [sub] = getSubjectTrialData(filePath)
% Returns a list of objects from the directory that's passed

subsdir = dir(filePath);
isub = [subsdir(:).isdir];
sub = {subsdir(isub).name}';
sub(ismember(sub,{'.','..','maxemg'})) = []; 
